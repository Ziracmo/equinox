import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LastRoundComponent} from './pages/last-round/last-round.component';
import {AdminComponent} from './pages/admin/admin.component';
import {AddPlayerComponent} from './components/organisms/add-player/add-player.component';
import {AddThemeComponent} from './components/organisms/add-theme/add-theme.component';
import {AddQuestionComponent} from './components/organisms/add-question/add-question.component';
import {LoginComponent} from './pages/login/login.component';
import {AuthGuard} from './guards/auth.guard';
import {HomeComponent} from './pages/home/home.component';
import {GamesComponent} from './pages/games/games.component';
import {CreatorComponent} from './pages/creator/creator.component';
import { SignupComponent } from './pages/signup/signup.component';


const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    component: AdminComponent,
    children: [
      {
        path: 'add-player',
        component: AddPlayerComponent
      },
      {
        path: 'add-theme',
        component: AddThemeComponent
      },
      {
        path: 'add-question',
        component: AddQuestionComponent
      }
    ]
  },
  {
    path: 'partie-3',
    canActivate: [AuthGuard],
    component: LastRoundComponent,
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'games',
    component: GamesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'creator',
    component: CreatorComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
