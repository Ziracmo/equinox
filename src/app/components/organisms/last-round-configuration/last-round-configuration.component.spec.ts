import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastRoundConfigurationComponent } from './last-round-configuration.component';

describe('LastRoundConfigurationComponent', () => {
  let component: LastRoundConfigurationComponent;
  let fixture: ComponentFixture<LastRoundConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastRoundConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastRoundConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
