import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LastRoundConfigurationComponent } from './last-round-configuration.component';



@NgModule({
    declarations: [LastRoundConfigurationComponent],
    exports: [
        LastRoundConfigurationComponent
    ],
    imports: [
        CommonModule
    ]
})
export class LastRoundConfigurationModule { }
