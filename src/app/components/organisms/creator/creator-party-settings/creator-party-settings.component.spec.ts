import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorPartySettingsComponent } from './creator-party-settings.component';

describe('CreatorPartySettingsComponent', () => {
  let component: CreatorPartySettingsComponent;
  let fixture: ComponentFixture<CreatorPartySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatorPartySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorPartySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
