import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import BulmaTagsInput from '@creativebulma/bulma-tagsinput';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-creator-party-settings',
  templateUrl: './creator-party-settings.component.html',
  styleUrls: ['./creator-party-settings.component.scss']
})
export class CreatorPartySettingsComponent implements OnInit {

  @Output()
  public onFormValid = new EventEmitter<any>();

  public form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      players: ['', Validators.required]
    });
    this.form.statusChanges
      .pipe(
        filter(() => this.form.valid))
      .subscribe(() => this.onFormValid.emit(this.form.value));
  }

  ngOnInit(): void {
    BulmaTagsInput.attach();
  }

  public handlePlayersChange(event) {
    this.form.patchValue({
      players: event.target.value.split(',')
    });
  }

}
