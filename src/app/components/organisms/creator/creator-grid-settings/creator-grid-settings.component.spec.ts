import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorGridSettingsComponent } from './creator-grid-settings.component';

describe('CreatorGridSettingsComponent', () => {
  let component: CreatorGridSettingsComponent;
  let fixture: ComponentFixture<CreatorGridSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatorGridSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorGridSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
