import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-creator-grid-settings',
  templateUrl: './creator-grid-settings.component.html',
  styleUrls: ['./creator-grid-settings.component.scss']
})
export class CreatorGridSettingsComponent implements OnInit {

  public themes = [];

  constructor() { }

  ngOnInit(): void {
  }

  addTheme(theme) {
    this.themes.push(theme);
  }

}
