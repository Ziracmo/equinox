import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorThemesSettingsComponent } from './creator-themes-settings.component';

describe('CreatorThemesSettingsComponent', () => {
  let component: CreatorThemesSettingsComponent;
  let fixture: ComponentFixture<CreatorThemesSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatorThemesSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorThemesSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
