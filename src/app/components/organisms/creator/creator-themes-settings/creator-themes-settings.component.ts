import {Component, OnInit} from '@angular/core';
import {faTrash} from '@fortawesome/free-solid-svg-icons';
import {Question} from '../../../../models/question';
import {Theme} from '../../../../models/theme';

@Component({
  selector: 'app-creator-themes-settings',
  templateUrl: './creator-themes-settings.component.html',
  styleUrls: ['./creator-themes-settings.component.scss']
})
export class CreatorThemesSettingsComponent implements OnInit {

  public isModalOpen = false;
  public selectedTheme: Theme;
  public themes: Theme[] = [];
  public trashIcon = faTrash;

  constructor() {
  }

  ngOnInit(): void {
  }

  public addTheme(theme: Theme) {
    this.isModalOpen = true;
    // TODO this.themes.push(theme);
    this.selectedTheme = theme;
  }

  public removeThemeFromList(index: number) {
    this.themes.splice(index, 1);
  }

  public closeModal() {
    this.isModalOpen = false;
  }

  public handleQuestionSelection(question: Question) {

  }

}
