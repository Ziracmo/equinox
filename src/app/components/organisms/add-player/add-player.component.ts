import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ThemesService} from '../../../services/themes.service';
import {PlayersService} from '../../../services/players.service';

@Component({
  selector: 'app-add-player',
  templateUrl: './add-player.component.html',
  styleUrls: ['./add-player.component.scss']
})
export class AddPlayerComponent implements OnInit {

  public form: FormGroup;
  public themes: any[];
  public players: any[];

  constructor(private themesService: ThemesService, private fb: FormBuilder, private playersService: PlayersService) {
    this.form = this.fb.group({
        name: ['', Validators.required],
        theme: ['', Validators.required]
      }
    );
  }

  ngOnInit(): void {
    this.themesService.getAllThemes().subscribe(res => this.themes = res);
    this.setPlayersData();
  }

  public addPlayer() {
    this.playersService.addPlayer(this.form.value).then(() => {
      this.form.reset();
      this.setPlayersData();
    });
  }

  private setPlayersData() {
    this.playersService.getAllPlayers().subscribe(res => {
      this.players = res;
    });
  }

  public deletePlayer(player) {
    this.playersService.deletePlayer(player).then(() => {
      this.setPlayersData();
    });
  }

}
