import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddPlayerComponent} from './add-player.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [AddPlayerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [AddPlayerComponent]
})
export class AddPlayerModule { }
