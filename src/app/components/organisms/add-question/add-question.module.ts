import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddQuestionComponent } from './add-question.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InputThemeAutocompleteModule} from '../../molecules/input-theme-autocomplete/input-theme-autocomplete.module';



@NgModule({
  declarations: [AddQuestionComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        InputThemeAutocompleteModule
    ]
})
export class AddQuestionModule { }
