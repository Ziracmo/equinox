import {Component, OnInit} from '@angular/core';
import {ThemesService} from '../../../services/themes.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuestionsService} from '../../../services/questions.service';

import * as Noty from 'noty';
import {Theme} from '../../../models/theme';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.scss']
})
export class AddQuestionComponent implements OnInit {

  public form: FormGroup;
  public themes: any[];
  public selectedThemes: Theme[] = [];

  constructor(private fb: FormBuilder, private questionsService: QuestionsService, private themesService: ThemesService) {
    this.form = this.fb.group({
      question: ['', Validators.required],
      answer: ['', Validators.required],
      themes: [[], Validators.required]
    });
  }

  ngOnInit(): void {
    this.themesService.getAllThemesChanges().subscribe(res => this.themes = res);
  }

  public addQuestion() {
    this.questionsService.addQuestion(this.form.value).then(() => {
      new Noty({
        text: 'Question correctement ajoutée',
      }).show();
      this.form.reset();
    });
  }

  public addTheme(theme: Theme) {
    if (!this.containsThemes(theme)) {
      this.selectedThemes.push(theme);
      this.form.patchValue({
        themes: this.selectedThemes.map(q => q.id)
      });
    }
  }

  public removeThemeFromQuestion(index: number) {
    this.selectedThemes.splice(index, 1);
    this.form.patchValue({
      themes: this.selectedThemes.map(q => q.id)
    });
  }

  public containsThemes(theme: Theme) {
    let i;
    for (i = 0; i < this.selectedThemes.length; i++) {
      if (this.selectedThemes[i].id === theme.id) {
        return true;
      }
    }
    return false;
  }

}
