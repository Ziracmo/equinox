import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ThemesService} from '../../../services/themes.service';
import {NotificationsService} from '../../../services/notifications.service';

@Component({
  selector: 'app-add-theme',
  templateUrl: './add-theme.component.html',
  styleUrls: ['./add-theme.component.scss']
})
export class AddThemeComponent implements OnInit {

  public form: FormGroup;
  public themes: any[];

  constructor(private themesService: ThemesService, private fb: FormBuilder, private notificationsService: NotificationsService) {
    this.form = this.fb.group({
      name: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.themesService.getAllThemes().subscribe(res => this.themes = res);
  }

  public addTheme() {
    this.themesService.addTheme(this.form.value).then(() => {
      this.themesService.getAllThemes().subscribe(res => this.themes = res);
      this.form.reset();
      this.notificationsService.showNotification('Thème correctement ajouté');
      this.form.reset();
    });
  }

  public deleteTheme(theme) {
    this.themesService.deleteTheme(theme).then(() => {
      this.themesService.getAllThemes().subscribe(res => this.themes = res);
    });
  }

}
