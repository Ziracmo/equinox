import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AddThemeComponent} from './add-theme.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [AddThemeComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class AddThemeModule {
}
