import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputQuestionAutocompleteComponent } from './input-theme-autocomplete.component';

describe('InputThemeAutocompleteComponent', () => {
  let component: InputQuestionAutocompleteComponent;
  let fixture: ComponentFixture<InputQuestionAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputQuestionAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputQuestionAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
