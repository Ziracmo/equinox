import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {InputQuestionAutocompleteComponent} from './input-question-autocomplete.component';

@NgModule({
  declarations: [InputQuestionAutocompleteComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [InputQuestionAutocompleteComponent]
})
export class InputQuestionAutocompleteModule { }
