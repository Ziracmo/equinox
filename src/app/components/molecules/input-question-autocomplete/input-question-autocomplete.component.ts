import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {QuestionsService} from '../../../services/questions.service';
import {Theme} from '../../../models/theme';
import {Question} from '../../../models/question';

@Component({
  selector: 'app-input-question-autocomplete',
  templateUrl: './input-question-autocomplete.component.html',
  styleUrls: ['./input-question-autocomplete.component.scss']
})
export class InputQuestionAutocompleteComponent implements OnInit {

  @Output()
  public questionSelected = new EventEmitter<any>();

  @Input()
  public theme: Theme;

  public autocompleteQuestions: Question[] = [];
  public query = '';

  constructor(private questionsService: QuestionsService) {
  }

  ngOnInit(): void {
  }

  public detectQuestions(event) {
    this.autocompleteQuestions = [];
    if (event.target.value && event.target.value !== '') {
      this.questionsService.getQuestionIncludingStringAndTheme(event.target.value, this.theme)
        .subscribe(res => this.autocompleteQuestions = res);
    }
  }

  public selectQuestion(question: Question) {
    this.query = '';
    this.autocompleteQuestions = [];
    this.questionSelected.emit(question);
  }


}
