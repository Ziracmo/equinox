import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ThemesService} from '../../../services/themes.service';

@Component({
  selector: 'app-input-theme-autocomplete',
  templateUrl: './input-theme-autocomplete.component.html',
  styleUrls: ['./input-theme-autocomplete.component.scss']
})
export class InputThemeAutocompleteComponent implements OnInit {

  @Output()
  public themeSelected = new EventEmitter<any>();

  public autocompleteThemes = [];
  public themeQuery = '';

  constructor(private themesService: ThemesService) {
  }

  ngOnInit(): void {
  }


  public detectTheme(event) {
    this.autocompleteThemes = [];
    if (event.target.value && event.target.value !== '') {
      this.themesService.getThemesIncludingString(event.target.value).subscribe(res => this.autocompleteThemes = res);
    }
  }

  public selectTheme(theme) {
    this.themeQuery = '';
    this.autocompleteThemes = [];
    this.themeSelected.emit(theme);
  }


}
