import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputThemeAutocompleteComponent } from './input-theme-autocomplete.component';

describe('InputThemeAutocompleteComponent', () => {
  let component: InputThemeAutocompleteComponent;
  let fixture: ComponentFixture<InputThemeAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputThemeAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputThemeAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
