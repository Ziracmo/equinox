import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InputThemeAutocompleteComponent} from './input-theme-autocomplete.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [InputThemeAutocompleteComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [InputThemeAutocompleteComponent]
})
export class InputThemeAutocompleteModule { }
