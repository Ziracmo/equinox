import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LastRoundComponent} from './pages/last-round/last-round.component';
import {AdminModule} from './pages/admin/admin.module';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {LoginModule} from './pages/login/login.module';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {GamesModule} from './pages/games/games.module';
import {CreatorModule} from './pages/creator/creator.module';
import {FormsModule} from '@angular/forms';
import {HomeModule} from './pages/home/home.module';
import { SignupModule } from './pages/signup/signup.module';

@NgModule({
  declarations: [
    AppComponent,
    LastRoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AdminModule,
    LoginModule,
    GamesModule,
    CreatorModule,
    HomeModule,
    SignupModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FontAwesomeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
