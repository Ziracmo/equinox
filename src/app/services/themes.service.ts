import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Theme} from '../models/theme';

@Injectable({
  providedIn: 'root'
})
export class ThemesService {

  private themeCollection: AngularFirestoreCollection;

  constructor(private afs: AngularFirestore) {
    this.themeCollection = this.afs.collection('themes');
  }

  public getAllThemes(): Observable<Theme[]> {
    return this.themeCollection.get().pipe(map(this.handleThemes));
  }

  public getAllThemesChanges(): Observable<any> {
    return this.themeCollection.valueChanges();
  }

  public getTheme(themeId: string): Observable<any> {
    return this.themeCollection.doc(themeId).get();
  }

  public addTheme(theme: any) {
    theme.name_lowercase = theme.name.toLowerCase();
    return this.themeCollection.add(theme);
  }

  public deleteTheme(theme: Theme) {
    return this.themeCollection.doc(theme.id).delete();
  }

  private handleThemes(snapshot): Theme[] {
    const themes = [];
    snapshot.forEach(doc => {
      const theme = doc.data();
      theme.id = doc.id;
      themes.push(theme);
    });
    return themes;
  }

  public getThemesIncludingString(query: string): Observable<Theme[]> {
    query = query.toLocaleLowerCase();
    return this.afs.collection('themes', ref => ref.orderBy('name_lowercase').startAt(query).endAt(query + '~').limit(7))
      .get().pipe(map(this.handleThemes));
  }
}
