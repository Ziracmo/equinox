import {Injectable} from '@angular/core';
import * as Noty from 'noty';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor() {
  }

  public showNotification(text, type: any = 'success'): void {
    new Noty({text, type, timeout: 3000}).show();
  }
}
