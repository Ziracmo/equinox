import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Question} from '../models/question';
import {Theme} from '../models/theme';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  private questionsCollection: AngularFirestoreCollection;

  constructor(private afs: AngularFirestore) {
    this.questionsCollection = this.afs.collection('questions');
  }

  public getAllQuestions(): Observable<Question[]> {
    return this.questionsCollection.get().pipe(map(this.handleQuestions));
  }

  public getAllQuestionsChanges(): Observable<any> {
    return this.questionsCollection.valueChanges();
  }

  public getQuestion(questionId: string) {
    return this.questionsCollection.doc(questionId).get();
  }

  public addQuestion(question: Question) {
    question.answer_lowercase = question.answer.toLowerCase();
    question.question_lowercase = question.question.toLowerCase();
    return this.questionsCollection.add(question);
  }

  private handleQuestions(snapshot): Question[] {
    const questions = [];
    snapshot.forEach(doc => {
      const question = doc.data();
      question.id = doc.id;
      questions.push(question);
    });
    return questions;
  }

  public getQuestionIncludingStringAndTheme(query: string, theme: Theme): Observable<Question[]> {
    query = query.toLocaleLowerCase();
    return this.afs.collection('questions', (ref) => {
      return ref
        .where('themes', 'array-contains', theme.id)
        .orderBy('question_lowercase')
        .startAt(query).endAt(query + '~')
        .limit(40);
    })
      .get().pipe(map(this.handleQuestions));
  }

}
