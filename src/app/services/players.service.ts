import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  private playerCollection: AngularFirestoreCollection;

  constructor(private afs: AngularFirestore) {
    this.playerCollection = this.afs.collection('players');
  }

  public getAllPlayers(): Observable<any> {
    return this.playerCollection.get().pipe(map((snapshot) => {
      const players = [];
      snapshot.forEach(doc => {
        const theme = doc.data();
        theme.id = doc.id;
        players.push(theme);
      });
      return players;
    }));
  }

  public getAllThemesChanges(): Observable<any> {
    return this.playerCollection.valueChanges();
  }

  public addPlayer(player) {
    return this.playerCollection.add(player);
  }

  public deletePlayer(player) {
    return this.playerCollection.doc(player.id).delete();
  }
}
