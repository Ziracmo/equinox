import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isAuthenticated = false;

  constructor(private afa: AngularFireAuth, private router: Router) {
    this.afa.authState.subscribe(res => {
      this.isAuthenticated = !!res;
    })
  }

  public get user() {
    return this.afa.auth.currentUser;
  }

  public emailLogin(email: string, password: string) {
    return this.afa.auth.signInWithEmailAndPassword(email, password);
  }

  public logout() {
    this.afa.auth.signOut().then(async () => {
      await this.router.navigateByUrl('/login');
    });
  }
  public signup(email: string, password: string) {
    return this.afa.auth.createUserWithEmailAndPassword(email, password)
  }
}
