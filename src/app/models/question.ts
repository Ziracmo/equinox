export class Question {
  question: string;
  'question_lowercase': string;
  answer: string;
  'answer_lowercase': string;
  themes: string[];
}
