import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatorComponent} from './creator.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CreatorThemesSettingsComponent} from '../../components/organisms/creator/creator-themes-settings/creator-themes-settings.component';
import {CreatorPartySettingsComponent} from '../../components/organisms/creator/creator-party-settings/creator-party-settings.component';
import {CreatorGridSettingsComponent} from '../../components/organisms/creator/creator-grid-settings/creator-grid-settings.component';
import {InputThemeAutocompleteModule} from '../../components/molecules/input-theme-autocomplete/input-theme-autocomplete.module';
import {InputQuestionAutocompleteModule} from '../../components/molecules/input-question-autocomplete/input-question-autocomplete.module';
import {WorkInProgressModule} from '../../components/molecules/work-in-progress/work-in-progress.module';


@NgModule({
  declarations: [
    CreatorComponent,
    CreatorThemesSettingsComponent,
    CreatorPartySettingsComponent,
    CreatorGridSettingsComponent,
  ],
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        InputThemeAutocompleteModule,
        InputQuestionAutocompleteModule,
        WorkInProgressModule
    ]
})
export class CreatorModule {
}
