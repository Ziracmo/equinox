import {Component, OnInit} from '@angular/core';
import * as bulmaSteps from 'bulma-steps';

@Component({
  selector: 'app-creator',
  templateUrl: './creator.component.html',
  styleUrls: ['./creator.component.scss']
})
export class CreatorComponent implements OnInit {

  public currentPage = 1;
  public isPartySettingsFormValid = false;
  public isPartyFractureFormValid = false;
  public isPartyGridFormValid = false;
  public partySettings: any;
  public partyFractureData: any;
  public partyGridDate: any;

  ngOnInit(): void {
    bulmaSteps.attach();
  }

  public onNext() {
    this.currentPage++;
  }

  public onPrev() {
    this.currentPage--;
  }

  public handlePartySettingsValidation(data): void {
    this.partySettings = data;
    this.isPartySettingsFormValid = true;
  }

  public handlePartyFractureValidation(data): void {
    this.partyFractureData = data;
    this.isPartyFractureFormValid = true;
  }

  public handlePartyGridValidation(data): void {
    this.partyGridDate = data;
    this.isPartyGridFormValid = true;
  }
}
