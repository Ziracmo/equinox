import {Component, OnInit, ViewChild} from '@angular/core';
import {QuestionsService} from '../../services/questions.service';
import {ThemesService} from '../../services/themes.service';

@Component({
  selector: 'app-last-round',
  templateUrl: './last-round.component.html',
  styleUrls: ['./last-round.component.scss']
})
export class LastRoundComponent implements OnInit {

  public modalOpen = false;
  public questionSelected: number;
  public questions = [];
  public themes = [];
  public isRevealed = false;
  public hasBeenRevealed = false;
  public timeLeft = 20;
  public displayAnswer = false;

  constructor(private questionsService: QuestionsService, private themesService: ThemesService) {
  }

  ngOnInit(): void {
    this.questionsService.getAllQuestions().subscribe(res => this.questions = this.shuffle(res));
    this.themesService.getAllThemes().subscribe(res => this.themes = res);
  }

  public openModal(index: number) {
    this.modalOpen = true;
    this.questionSelected = index;
    this.questions[index].visited = true;
  }

  public closeModal() {
    this.modalOpen = false;
  }


  private shuffle(array) {
    // tslint:disable-next-line:one-variable-per-declaration
    let counter = array.length, temp, index;

    while (counter > 0) {
      index = Math.floor(Math.random() * counter);
      counter--;
      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
    }
    return array;
  }

  public revealCard() {
    this.isRevealed = !this.isRevealed;
    this.hasBeenRevealed = true;
    setInterval(() => {
      this.timeLeft--;
      if (this.timeLeft <= 0) {
        this.isRevealed = false;
      }
    }, 1000);
  }

  public getThemeIndex(theme) {
    const ng = this.themes.find(t => t.name === theme);
    return this.themes.indexOf(ng);
  }

  public toggleAnswer() {
    this.displayAnswer = !this.displayAnswer;
  }

}
