import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastRoundComponent } from './last-round.component';

describe('LastRoundComponent', () => {
  let component: LastRoundComponent;
  let fixture: ComponentFixture<LastRoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastRoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastRoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
