import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminComponent} from './admin.component';
import {RouterModule} from '@angular/router';
import {LastRoundConfigurationModule} from '../../components/organisms/last-round-configuration/last-round-configuration.module';
import {AddPlayerModule} from '../../components/organisms/add-player/add-player.module';
import {AddThemeModule} from '../../components/organisms/add-theme/add-theme.module';
import {AddQuestionModule} from '../../components/organisms/add-question/add-question.module';

@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    RouterModule,
    LastRoundConfigurationModule,
    AddPlayerModule,
    AddThemeModule,
    AddQuestionModule
  ]
})
export class AdminModule {
}
