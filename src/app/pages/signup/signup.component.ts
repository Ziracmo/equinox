import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  
  public form: FormGroup;
  public invalidPwd = false;

  constructor(private authService: AuthService, private fb: FormBuilder, private router: Router) {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirm: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  public async signup() {
    const {email,password,confirm} = this.form.value;
    if (password === confirm){
      this.invalidPwd = false;
      const res =  await this.authService.signup(email,password);
      if (res.user) {
        await this.router.navigateByUrl('/home');
      }
    }
    else{
      this.invalidPwd = true;
    }
  }

  
  
}
