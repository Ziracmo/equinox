import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {AppModule} from '../../app.module';
import {WorkInProgressModule} from '../../components/molecules/work-in-progress/work-in-progress.module';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    WorkInProgressModule
  ]
})
export class HomeModule {
}
