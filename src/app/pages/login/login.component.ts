import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;

  constructor(private authService: AuthService, private fb: FormBuilder, private router: Router) {
    this.form = fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  public async login() {
    const {email, password} = this.form.value;
    const res = await this.authService.emailLogin(email, password);
    if (res.user) {
      await this.router.navigateByUrl('/home');
    }
  }
}
