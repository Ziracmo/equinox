import {Component} from '@angular/core';
import {faHatWizard, faHome, faTools} from '@fortawesome/free-solid-svg-icons';
import {AuthService} from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public faHome = faHome;

  public faGames = faHatWizard;
  public faAdmin = faTools;

  constructor(public authService: AuthService) {
  }

  public toggleMenu() {
  }
}
