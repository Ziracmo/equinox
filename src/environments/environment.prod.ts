export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCdHxuyzyaCsj6EE9r2V7lybz32UQAVyHY',
    authDomain: 'equinox-game.firebaseapp.com',
    databaseURL: 'https://equinox-game.firebaseio.com',
    projectId: 'equinox-game',
    storageBucket: 'equinox-game.appspot.com',
    messagingSenderId: '12957449815',
    appId: '1:12957449815:web:10d60e2c82ff45de92ed12',
    measurementId: 'G-0723V8K1QP'
  }
};
