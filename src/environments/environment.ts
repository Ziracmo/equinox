// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCdHxuyzyaCsj6EE9r2V7lybz32UQAVyHY',
    authDomain: 'equinox-game.firebaseapp.com',
    databaseURL: 'https://equinox-game.firebaseio.com',
    projectId: 'equinox-game',
    storageBucket: 'equinox-game.appspot.com',
    messagingSenderId: '12957449815',
    appId: '1:12957449815:web:10d60e2c82ff45de92ed12',
    measurementId: 'G-0723V8K1QP'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
