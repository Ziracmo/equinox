import * as functions from 'firebase-functions';

const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

export const incrementQuestionsCounter = functions.firestore.document('questions/{questionId}').onCreate((event) => {
  console.log('Detection de la création d\'une question');
  return db.collection('core_data').doc('questions_data').get().then((doc: any) => {
    const actualNumber = doc.data().number + 1;
    db.collection('core_data').doc('questions_data').update({
      number: actualNumber
    }).then(() => {
      console.log('Incrémentation de la valeur number du document question_data', actualNumber);
    })
    return actualNumber;
  });
});

export const decrementQuestionsCounter = functions.firestore.document('questions/{questionId}').onDelete(() => {
  console.log('Detection de la supression d\'une question');
  return db.collection('core_data').doc('questions_data').get().then((doc: any) => {
    const actualNumber = doc.data().number - 1;
    db.collection('core_data').doc('questions_data').update({
      number: actualNumber
    }).then(() => {
      console.log('Décrémentation de la valeur number du document question_data', actualNumber);
    })
    return actualNumber;
  });
});
